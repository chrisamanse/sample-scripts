# Sample Scripts

Scripts made from exercises for bash scripting.

## calculator.sh
  - Computes expressions with two parameters and one operator
  - Operators valid are: `+ - * / % == !=`
  - To use the multiply operator, `*` should be escaped such as `\*` or `"*"` or `'*'`
    - since `*` is treated as a wild card and outputs file names in current directory
  - Usage:
    - $ ./calculator.sh [parameter1] [operator] [parameter2]
    - $ ./calculator.sh 4 + 5
    - $ 9

## flattenDirectory.sh
  - flattens a directory such that all files inside the folder and its nests of subfolders are moved to the parent folder
  - includes interface to ask user whether to overwrite files if same file name
    - from mv command
  - move includes hidden files (starting with `.`)
    - from find command
  - Usage:
    - Folder Structure:
      - ParentFolder/
        - file1
        - file2
        - SubFolder1/
          - file3
          - SubFolder2/
            - file4
        - SubFolder3
          - file5
    - $ ./flattenDirectory ParentFolder/
    - Folder Structure:
      - ParentFolder/
        - file1
        - file2
        - file3
        - file4
        - file5
        - SubFolder1/
          - SubFolder2/
        - SubFolder3

## isPrimeOrComposite.sh
  - checks if number is prime or composite
  - if composite, outputs its factors
  - Usage:
    - $ ./isPrimeOrComposite 7
    - $ 7 is prime
    - $ ./isPrimeOrComposite 10
    - $ 10 is composite
    - $ Its factors are:
    - $ 1 2 5 10

## multipleRename.sh
  - renames multiple files to file name pattern [filename][count].[file extension]
  - Usage:
    - ./multipleRename.sh [TARGET FILE NAME] [FILE]…
    - $ ./multipleRename.sh testName.txt TestFolder/a.sh TestFolder/b.sh
    - $ TestFolder/a.sh > testName0.txt
    - $ TestFolder/b.sh > testName1.txt

## rgbHexDecConvert.sh
  - converts RGB Hexadecimal to decimal and vice versa
  - if one argument is given, scripts assumes hex to dec
  - if three arguments, dec to hex
  - Usage:
    - $ ./rgbHexDecConvert.sh FFFFFF
    - $ 255 255 255
    - $ ./rgbHexDecConvert.sh 255 255 0
    - $ FFFF00

## License

Copyright (c) 2015 Joe Christopher Paul Amanse. This software is distributed under the [MIT License](./LICENSE.md).