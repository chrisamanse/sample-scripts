#!/bin/sh
# USAGE:
# ./multipleRename.sh [TARGET] [FILE]...

# Get target
FILE_NAME=$1
FILE_EXT=${FILE_NAME##*.}
FILE_NOEXT=${FILE_NAME%.$FILE_EXT}

i=0
for file in $*
do
  if [ $FILE_NAME != $file ]
  then
    # if file has no extension
    if [ $FILE_NOEXT == $FILE_EXT ]
    then
      target="$FILE_NOEXT$i"
    else
      target="$FILE_NOEXT$i.$FILE_EXT"
    fi
    
    echo "$file > $target"
    mv "$file" "$target" 
    i=$((i + 1))
  fi
done
