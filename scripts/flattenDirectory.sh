#!/bin/sh

targetDirectory=$1

foundFiles=`find $targetDirectory -mindepth 2 -type f`

for file in $foundFiles
do
  mv -i -v "$file" "$targetDirectory"
done
