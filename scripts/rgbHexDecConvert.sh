#!/bin/sh
# USAGE:
# ./rgbHexDecConvert [HEX]
# or
# ./rgbHexDecConvert [DEC Red] [DEC Green] [DEC Blue]

if [ $# == 1 ]
then
  #Convert input to uppercase to be sure
  hex=$1
  hexR=${hex:0:2}
  hexG=${hex:2:2}
  hexB=${hex:4:2}

  decR=`printf "%d" 0x$hexR`
  decG=`printf "%d" 0x$hexG`
  decB=`printf "%d" 0x$hexB`
  echo $decR $decG $decB
else
if [ $# == 3 ]
then
  hexR=`printf "%02X" $1`
  hexG=`printf "%02X" $2`
  hexB=`printf "%02X" $3`
  echo "$hexR$hexG$hexB"
fi
fi
