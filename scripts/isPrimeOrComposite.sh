#!/bin/sh

inputNumber=$1
div=2

factors="1 "
isPrime=1
while [ $div -lt $inputNumber ]
do
  mod=$((inputNumber % div))

  if [ $mod == 0 ]
  then
    isPrime=0
    factors="$factors$div "
  fi
  
  div=$((div+1))
done

if [ $isPrime == 0 ]
then
  echo "$inputNumber is composite"
  
  # Add self
  factors="$factors$inputNumber "
  echo "Its factors are: "
  echo "$factors"
else
  echo "$inputNumber is prime"
fi
