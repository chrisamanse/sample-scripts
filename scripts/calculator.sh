#!/bin/sh

firstParam=$1
operator=$2
secondParam=$3

case $operator in
  "+")
  answer=$((firstParam + secondParam))
  ;;
  "-")
  answer=$((firstParam - secondParam))
  ;;
  "*")
  answer=$((firstParam * secondParam))
  ;;
  "/")
  answer=$((firstParam / secondParam))
  ;;
  "%")
  answer=$((firstParam % secondParam))
  ;;
  "==")
  answer=$((firstParam == secondParam))
  ;;
  "!=")
  answer=$((firstParam != secondParam))
  ;;
  *)
  echo "Unknown operator"
  answer=0
  ;;
esac

echo $answer
